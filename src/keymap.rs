use ahash::AHashMap;
use core::convert::TryFrom;
use core::mem::{size_of, size_of_val};
use core::num;
use redox_input::{KeyModifier, Keycode};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::io;

#[cfg(feature = "test")]
pub const KEYMAPS_PATH: &str = "./res/";

#[cfg(not(feature = "test"))]
pub const KEYMAPS_PATH: &str = "/etc/keymaps/";

#[derive(Debug)]
pub enum LoadingError {
    IO(io::Error),
    Deserialization(toml::de::Error),
    InvalidValues,
    ParseInt(num::ParseIntError, String),
}

/// The entry type to the internal maps of a [Keymap].
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
struct KeymapEntry {
    keycode: Keycode,
    modifier: KeyModifier,
}

impl KeymapEntry {
    /// Creates a new non-modified entry.
    fn empty(keycode: Keycode) -> Self {
        Self::new(keycode, KeyModifier::empty())
    }

    /// Creates a new entry with given modifiers.
    fn new(keycode: Keycode, modifier: KeyModifier) -> Self {
        Self { keycode, modifier }
    }
}

/// A keymap is a dictionary of [Keycodes](Keycode), [chars](char) with possible
/// [modifiers](KeyModifier) like `Shift`, `Alt` and `AltGr` (left/right).
#[derive(Debug)]
pub struct Keymap {
    keycodes: AHashMap<KeymapEntry, Keycode>,
    chars: AHashMap<KeymapEntry, char>,
    pub path: String,
}

impl Default for Keymap {
    /// Loads the default backup keymap (US layout)
    fn default() -> Self {
        Self::backup()
    }
}

impl Keymap {
    /// Replaces the contents of this keymap with the other's.
    pub fn replace_with(&mut self, other: Keymap) {
        self.path = other.path;
        self.keycodes = other.keycodes;
        self.chars = other.chars;
    }

    /// Returns the theoretical number of bytes used for the internal
    /// - keycode->keycode mappings
    /// - keycode->char mappings
    /// - path
    #[cold]
    pub fn mem_size(&self) -> usize {
        let a = self.keycodes.len() * (size_of::<KeymapEntry>() + size_of::<Keycode>());
        let b = self.chars.len() * (size_of::<KeymapEntry>() + size_of::<char>());
        let c = size_of_val(&self.path);

        a + b + c
    }

    /// Returns the backup keymap that is statically included in this library.
    ///
    /// The backup keymap is the traditional US layout.
    #[allow(dead_code)]
    #[cold]
    pub fn backup() -> Self {
        let backup = include_str!("../res/backup.toml");
        Self::load_file_content(backup, "backup").expect("backup keymap corrupt")
    }

    pub fn path_of(keymap: &str) -> String {
        format!("{}{}.toml", KEYMAPS_PATH, keymap)
    }

    /// Returns the number of [Keycode] mappings.
    pub fn num_keycodes(&self) -> usize {
        self.keycodes.len()
    }

    /// Returns the number of [char]] mappings.
    pub fn num_chars(&self) -> usize {
        self.chars.len()
    }

    /// Clears all internal maps.
    pub fn clear(&mut self) {
        self.keycodes.clear();
        self.chars.clear();
    }

    /// Shrinks the capacity of the internal maps as much as possible.
    fn shrink_to_fit(&mut self) {
        self.keycodes.shrink_to_fit();
        self.chars.shrink_to_fit();
    }

    /// Merges this keymap with the other one, overwriting any existing key with the other's
    /// definition.
    fn merge(&mut self, mut other: Self) {
        for (key, val) in other.keycodes.drain() {
            self.keycodes.insert(key, val);
        }
        for (key, val) in other.chars.drain() {
            self.chars.insert(key, val);
        }
    }

    /// Attempts to get the [Keycode] mapped to the given keycode and [modifier](KeyModifier).
    ///
    /// # Returns
    /// - `None`: if the keycode + modifier is not found in the keymap.
    /// - `Some(Keycode)`: the found keycode in the keymap
    #[inline] // improves performance (empirically proven)
    pub fn get_keycode(&self, keycode: Keycode, modifier: KeyModifier) -> Option<Keycode> {
        let entry = KeymapEntry::new(keycode, modifier);
        self.keycodes.get(&entry).copied()
    }

    /// Attempts to get the [char] mapped to the given keycode and [modifier](KeyModifier).
    ///
    /// # Returns
    /// - `None`: if the keycode + modifier is not found in the keymap.
    /// - `Some(char)`: the found char in the keymap
    #[inline] // improves performance (empirically proven)
    pub fn get_char(&self, keycode: Keycode, modifier: KeyModifier) -> Option<char> {
        let entry = KeymapEntry::new(keycode, modifier);
        self.chars.get(&entry).copied()
    }

    /// Loads a [Keymap] from the given [path](Path).
    ///
    /// # Arguments
    /// - `path`: The full path of the keymap
    ///
    /// # Errors
    /// This function will return an error if
    /// - the path does not exist or cannot be opened/read, or
    /// - the content cannot be deserialized into a keymap, or
    /// -
    /// - [`IO`](LoadingError::IO): the path does not exist or cannot be opened/read.
    /// - [`Deserialization`](LoadingError::Deserialization): If the content cannot be deserialized into a keymap
    /// - [`ParseInt`](LoadingError::ParseInt): If a value inside the deserialization cannot
    ///
    /// # Returns
    /// - `Ok(Keymap)`: if successful.
    /// - `Err(e)`: if an [error](LoadingError) occurs. The returned error includes some details.
    pub fn load_keymap(path: &str) -> Result<Self, LoadingError> {
        let file_content = std::fs::read_to_string(path).map_err(LoadingError::IO)?;

        Self::load_file_content(&file_content, path)
    }

    pub fn load_file_content(content: &str, path: &str) -> Result<Self, LoadingError> {
        let toml: KeymapTOML = toml::from_str(content).map_err(LoadingError::Deserialization)?;

        let mut keymap = Self {
            keycodes: Default::default(),
            chars: Default::default(),
            path: path.into(),
        };

        // println!("{}", content);

        // Merge includes
        for include in &toml.include {
            let other = Self::load_keymap(&Self::path_of(include))?;
            keymap.merge(other);
        }

        Self::load_keycodes(&mut keymap, &toml)?;
        Self::load_chars(&mut keymap, &toml)?;

        keymap.shrink_to_fit();

        Ok(keymap)
    }

    /// Transfers the [keycodes](Keycode) of the [toml](KeymapTOML) into the [Keymap].
    fn load_keycodes(keymap: &mut Self, toml: &KeymapTOML) -> Result<(), LoadingError> {
        // Load keymap
        for (key, target) in &toml.keycodes {
            let keycode = Keycode::try_from(key.as_ref())
                .map_err(|e| LoadingError::ParseInt(e, key.clone()))?;

            keymap.keycodes.insert(
                KeymapEntry::empty(keycode),
                Keycode::from(*target.no_shift()),
            );

            if let Some(&shift) = target.shift() {
                keymap.keycodes.insert(
                    KeymapEntry::new(keycode, KeyModifier::SHIFT),
                    Keycode::from(shift),
                );
            }
        }

        // Load modifiers
        for (target, modifiers) in &toml.modifiers.keycode {
            let target = Keycode::try_from(target.as_ref())
                .map_err(|e| LoadingError::ParseInt(e, target.clone()))?;

            keymap
                .keycodes
                .insert(Self::parse_modifiers(modifiers)?, target);
        }

        Ok(())
    }

    /// Transfers the unicode [chars](char) of the [toml](KeymapTOML) into the [Keymap].
    fn load_chars(keymap: &mut Self, toml: &KeymapTOML) -> Result<(), LoadingError> {
        // Load keymap
        for (key, targets) in &toml.chars {
            let key = Keycode::try_from(key.as_ref())
                .map_err(|e| LoadingError::ParseInt(e, key.clone()))?;

            keymap
                .chars
                .insert(KeymapEntry::empty(key), *targets.no_shift());

            if let Some(&shift) = targets.shift() {
                keymap
                    .chars
                    .insert(KeymapEntry::new(key, KeyModifier::SHIFT), shift);
            }
        }

        // Load modifiers
        for (target, modifiers) in &toml.modifiers.chars {
            keymap
                .chars
                .insert(Self::parse_modifiers(modifiers)?, *target);
        }

        Ok(())
    }

    /// Tries to parse the given [String] buffer into a [KeymapEntry].
    ///
    /// # Summary
    /// The modifiers must contain
    /// - zero or more [modifiers](KeyModifier)
    /// - exactly one [Keycode]
    ///
    /// # Returns
    /// - [KeymapEntry]: if all is good.
    /// - [LoadingError::InvalidValues]: If not exactly one [Keycode] is given.
    fn parse_modifiers(modifiers: &[String]) -> Result<KeymapEntry, LoadingError> {
        let mut keycode = None;
        let mut modifier = KeyModifier::empty();

        for value in modifiers.iter().map(AsRef::as_ref) {
            if let Ok(m) = KeyModifier::try_from(value) {
                modifier.insert(m);
            } else if keycode.is_some() {
                return Err(LoadingError::InvalidValues);
            } else {
                let kc = Keycode::try_from(value)
                    .map_err(|e| LoadingError::ParseInt(e, value.into()))?;
                keycode = Some(kc);
            }
        }

        let keycode = keycode.ok_or(LoadingError::InvalidValues)?;
        Ok(KeymapEntry::new(keycode, modifier))
    }
}

/// The possible entry for `[keycode]` and `[char]` maps inside a [KeymapTOML].
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
enum TOMLEntry<T> {
    /// A non-shifted value.
    NoShift(T),
    /// A non-shifted and shifted value.
    Shift(T, T),
}

impl<T> TOMLEntry<T> {
    /// Returns the non-shifted content.
    fn no_shift(&self) -> &T {
        match self {
            TOMLEntry::NoShift(t) => t,
            TOMLEntry::Shift(t, _) => t,
        }
    }

    /// Returns the shifted content if any.
    fn shift(&self) -> Option<&T> {
        match self {
            TOMLEntry::Shift(_, t) => Some(t),
            _ => None,
        }
    }
}

/// A mapping of modifiers for the [KeymapTOML].
#[derive(Clone, Debug, Default, Deserialize, Serialize)]
struct ModifiersTOML {
    /// The optional mappings of `keycode <= [list, of, modifiers, and, key]`
    #[serde(default)]
    pub keycode: HashMap<String, Vec<String>>,
    /// The optional mappings of `unicode <= [list, of, modifiers, and, key]`
    #[serde(default)]
    pub chars: HashMap<char, Vec<String>>,
}

/// A keymap for de-/serialization.
#[derive(Clone, Debug, Default, Deserialize, Serialize)]
struct KeymapTOML {
    /// The keymaps to include beforehand
    #[serde(default)]
    pub include: Vec<String>,
    /// The mappings of `keycode => [key, shift+key)]` as [Keycode]
    #[serde(default)]
    pub keycodes: HashMap<String, TOMLEntry<u16>>,
    /// The mappings of `keycode => [key, shift+key)]` as [Unicode]
    #[serde(default)]
    pub chars: HashMap<String, TOMLEntry<char>>,
    #[serde(default)]
    pub modifiers: ModifiersTOML,
}
