#![cfg_attr(test, allow(dead_code))]

pub mod keymap;

pub use keymap::*;
#[cfg(feature = "test")]
pub use test_utilities::*;

#[cfg(feature = "test")]
pub mod test_utilities {
    use crate::keymap::Keymap;
    use core::ops::RangeInclusive;
    use redox_input::{KeyModifier, Keycode, RegularKeycode};

    /// Returns a-z keycodes
    pub const fn abc_keys() -> [Keycode; 26] {
        [
            Keycode::Regular(RegularKeycode::KeyA),
            Keycode::Regular(RegularKeycode::KeyB),
            Keycode::Regular(RegularKeycode::KeyC),
            Keycode::Regular(RegularKeycode::KeyD),
            Keycode::Regular(RegularKeycode::KeyE),
            Keycode::Regular(RegularKeycode::KeyF),
            Keycode::Regular(RegularKeycode::KeyG),
            Keycode::Regular(RegularKeycode::KeyH),
            Keycode::Regular(RegularKeycode::KeyI),
            Keycode::Regular(RegularKeycode::KeyJ),
            Keycode::Regular(RegularKeycode::KeyK),
            Keycode::Regular(RegularKeycode::KeyL),
            Keycode::Regular(RegularKeycode::KeyM),
            Keycode::Regular(RegularKeycode::KeyN),
            Keycode::Regular(RegularKeycode::KeyO),
            Keycode::Regular(RegularKeycode::KeyP),
            Keycode::Regular(RegularKeycode::KeyQ),
            Keycode::Regular(RegularKeycode::KeyR),
            Keycode::Regular(RegularKeycode::KeyS),
            Keycode::Regular(RegularKeycode::KeyT),
            Keycode::Regular(RegularKeycode::KeyU),
            Keycode::Regular(RegularKeycode::KeyV),
            Keycode::Regular(RegularKeycode::KeyW),
            Keycode::Regular(RegularKeycode::KeyX),
            Keycode::Regular(RegularKeycode::KeyY),
            Keycode::Regular(RegularKeycode::KeyZ),
        ]
    }

    /// Returns 1-9,0 keycodes
    pub const fn number_keys() -> [Keycode; 10] {
        [
            Keycode::Regular(RegularKeycode::Key1),
            Keycode::Regular(RegularKeycode::Key2),
            Keycode::Regular(RegularKeycode::Key3),
            Keycode::Regular(RegularKeycode::Key4),
            Keycode::Regular(RegularKeycode::Key5),
            Keycode::Regular(RegularKeycode::Key6),
            Keycode::Regular(RegularKeycode::Key7),
            Keycode::Regular(RegularKeycode::Key8),
            Keycode::Regular(RegularKeycode::Key9),
            Keycode::Regular(RegularKeycode::Key0),
        ]
    }

    /// Returns Esc, F1-F12, Pause, F13-F24
    pub const fn function_keys() -> [Keycode; 28] {
        [
            Keycode::Regular(RegularKeycode::KeyEsc),
            Keycode::Regular(RegularKeycode::KeyF1),
            Keycode::Regular(RegularKeycode::KeyF2),
            Keycode::Regular(RegularKeycode::KeyF3),
            Keycode::Regular(RegularKeycode::KeyF4),
            Keycode::Regular(RegularKeycode::KeyF5),
            Keycode::Regular(RegularKeycode::KeyF6),
            Keycode::Regular(RegularKeycode::KeyF7),
            Keycode::Regular(RegularKeycode::KeyF8),
            Keycode::Regular(RegularKeycode::KeyF9),
            Keycode::Regular(RegularKeycode::KeyF10),
            Keycode::Regular(RegularKeycode::KeyF11),
            Keycode::Regular(RegularKeycode::KeyF12),
            Keycode::Regular(RegularKeycode::KeyPrintScreen),
            Keycode::Regular(RegularKeycode::KeyScrollLock),
            Keycode::Regular(RegularKeycode::KeyPause),
            Keycode::Regular(RegularKeycode::KeyF13),
            Keycode::Regular(RegularKeycode::KeyF14),
            Keycode::Regular(RegularKeycode::KeyF15),
            Keycode::Regular(RegularKeycode::KeyF16),
            Keycode::Regular(RegularKeycode::KeyF17),
            Keycode::Regular(RegularKeycode::KeyF18),
            Keycode::Regular(RegularKeycode::KeyF19),
            Keycode::Regular(RegularKeycode::KeyF20),
            Keycode::Regular(RegularKeycode::KeyF21),
            Keycode::Regular(RegularKeycode::KeyF22),
            Keycode::Regular(RegularKeycode::KeyF23),
            Keycode::Regular(RegularKeycode::KeyF24),
        ]
    }

    /// Returns numpad keys
    pub const fn numpad_keys() -> [Keycode; 17] {
        [
            Keycode::Regular(RegularKeycode::KeypadNumLock),
            Keycode::Regular(RegularKeycode::KeypadSlash),
            Keycode::Regular(RegularKeycode::KeypadStar),
            Keycode::Regular(RegularKeycode::KeypadMinus),
            Keycode::Regular(RegularKeycode::KeypadPlus),
            Keycode::Regular(RegularKeycode::KeypadEnter),
            Keycode::Regular(RegularKeycode::Keypad1),
            Keycode::Regular(RegularKeycode::Keypad2),
            Keycode::Regular(RegularKeycode::Keypad3),
            Keycode::Regular(RegularKeycode::Keypad4),
            Keycode::Regular(RegularKeycode::Keypad5),
            Keycode::Regular(RegularKeycode::Keypad6),
            Keycode::Regular(RegularKeycode::Keypad7),
            Keycode::Regular(RegularKeycode::Keypad8),
            Keycode::Regular(RegularKeycode::Keypad9),
            Keycode::Regular(RegularKeycode::Keypad0),
            Keycode::Regular(RegularKeycode::KeypadDot),
        ]
    }

    /// Returns navigation and arrow keys
    pub const fn navigation_arrows_keys() -> [Keycode; 10] {
        [
            Keycode::Regular(RegularKeycode::KeyInsert),
            Keycode::Regular(RegularKeycode::KeyHome),
            Keycode::Regular(RegularKeycode::KeyPageUp),
            Keycode::Regular(RegularKeycode::KeyDeleteForward),
            Keycode::Regular(RegularKeycode::KeyEnd),
            Keycode::Regular(RegularKeycode::KeyPageDown),
            Keycode::Regular(RegularKeycode::KeyRightArrow),
            Keycode::Regular(RegularKeycode::KeyLeftArrow),
            Keycode::Regular(RegularKeycode::KeyDownArrow),
            Keycode::Regular(RegularKeycode::KeyUpArrow),
        ]
    }

    /// Returns modifier keys
    pub const fn modifier_keys() -> [Keycode; 13] {
        [
            Keycode::Regular(RegularKeycode::KeyReturnEnter),
            Keycode::Regular(RegularKeycode::KeyDel),
            Keycode::Regular(RegularKeycode::KeyTab),
            Keycode::Regular(RegularKeycode::KeyCapsLock),
            Keycode::Regular(RegularKeycode::KeyApplication),
            Keycode::Regular(RegularKeycode::KeyLeftControl),
            Keycode::Regular(RegularKeycode::KeyLeftShift),
            Keycode::Regular(RegularKeycode::KeyLeftAlt),
            Keycode::Regular(RegularKeycode::KeyLeftGUI),
            Keycode::Regular(RegularKeycode::KeyRightControl),
            Keycode::Regular(RegularKeycode::KeyRightShift),
            Keycode::Regular(RegularKeycode::KeyRightAlt),
            Keycode::Regular(RegularKeycode::KeyRightGUI),
        ]
    }

    /// Tests a keymap keycode->keycode mappings against keycodes
    pub fn test_linear_keymappings(keymap: &Keymap, keys: &[Keycode], shift_as_well: bool) {
        match shift_as_well {
            true => assert!(keymap.num_keycodes() >= 2 * keys.len()),
            false => assert!(keymap.num_keycodes() >= keys.len()),
        }

        for &keycode in keys {
            assert_eq!(
                Some(keycode),
                keymap.get_keycode(keycode, KeyModifier::empty()),
            );

            if shift_as_well {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT),
                );
            }
        }
    }

    pub fn test_numbers(keymap: &Keymap) {
        test_linear_keymappings(keymap, &number_keys(), false);
    }

    pub fn test_function_row(keymap: &Keymap) {
        test_linear_keymappings(keymap, &function_keys(), false);
    }

    pub fn test_numpad(keymap: &Keymap) {
        test_linear_keymappings(keymap, &numpad_keys(), false);
    }

    pub fn test_navigation_arrows(keymap: &Keymap) {
        test_linear_keymappings(keymap, &navigation_arrows_keys(), false);
    }

    pub fn test_modifiers(keymap: &Keymap) {
        test_linear_keymappings(keymap, &modifier_keys(), false);
    }

    pub fn test_qwerty_layout(keymap: &Keymap) {
        let keys = abc_keys();
        assert!(keymap.num_keycodes() >= keys.len());

        for (idx, &keycode) in keys.iter().enumerate() {
            assert_eq!(
                Some(keycode),
                keymap.get_keycode(keycode, KeyModifier::empty()),
            );
            assert_eq!(
                Some(keycode),
                keymap.get_keycode(keycode, KeyModifier::SHIFT),
            );

            assert_eq!(
                char::from_u32(97 + idx as u32),
                keymap.get_char(keycode, KeyModifier::empty()),
            );
            assert_eq!(
                char::from_u32(65 + idx as u32),
                keymap.get_char(keycode, KeyModifier::SHIFT),
            );
        }
    }

    pub fn test_qwertz_layout(keymap: &Keymap) {
        let keys = abc_keys();
        assert!(keymap.num_keycodes() >= keys.len());

        for (idx, &keycode) in keys.iter().enumerate() {
            let expect = match keycode {
                Keycode::Regular(RegularKeycode::KeyY) => Keycode::Regular(RegularKeycode::KeyZ),
                Keycode::Regular(RegularKeycode::KeyZ) => Keycode::Regular(RegularKeycode::KeyY),
                _ => keycode,
            };

            assert_eq!(
                Some(expect),
                keymap.get_keycode(keycode, KeyModifier::empty())
            );
            assert_eq!(
                Some(expect),
                keymap.get_keycode(keycode, KeyModifier::SHIFT)
            );

            assert_eq!(
                char::from_u32(97 + idx as u32),
                keymap.get_char(keycode, KeyModifier::empty()),
            );
            assert_eq!(
                char::from_u32(65 + idx as u32),
                keymap.get_char(keycode, KeyModifier::SHIFT),
            );
        }
    }

    /// Tests all [Keymap::get_keycode()] to be [Option::None] apart from the given modifier
    /// exceptions.
    pub fn test_get_keycode_equals_none(
        keymap: &Keymap,
        keycode: Keycode,
        exceptions: &[KeyModifier],
    ) {
        const RANGE: RangeInclusive<u8> = KeyModifier::empty().bits()..=KeyModifier::all().bits();

        for i in RANGE {
            if exceptions.iter().any(|e| e.bits() == i) {
                continue;
            }

            let m = KeyModifier::from_bits(i).unwrap();

            assert!(keymap.get_keycode(keycode, m).is_none());
        }
    }

    /// Tests all [Keymap::get_char()] to be [Option::None] apart from the given modifier
    /// exceptions.
    pub fn test_get_char_equals_none(
        keymap: &Keymap,
        keycode: Keycode,
        exceptions: &[KeyModifier],
    ) {
        const RANGE: RangeInclusive<u8> = KeyModifier::empty().bits()..=KeyModifier::all().bits();

        for i in RANGE {
            if exceptions.iter().any(|e| e.bits() == i) {
                continue;
            }

            let m = KeyModifier::from_bits(i).unwrap();

            assert!(keymap.get_char(keycode, m).is_none());
        }
    }
}
