use criterion::{criterion_group, criterion_main, Criterion};
use redox_input::{KeyModifier, Keycode};
use redox_keymap::Keymap;
use std::time::Duration;

#[inline]
fn bench(map: &Keymap) -> (Option<Keycode>, Option<char>) {
    let keycode = Keycode::from(fastrand::u16(..));
    (
        map.get_keycode(keycode, KeyModifier::empty()),
        map.get_char(keycode, KeyModifier::empty()),
    )
}

pub fn keymap(c: &mut Criterion) {
    let path = "./res/us.toml";

    let map = Keymap::load_keymap(path).unwrap();
    c.bench_function("HashMap", |b| b.iter(|| bench(&map)));
}

criterion_group!(
    name = benches;
    config = Criterion::default()
                .warm_up_time(Duration::from_secs(5))
                .measurement_time(Duration::from_secs(10))
                .sample_size(500);
    targets = keymap
);
criterion_main!(benches);
