# Specification

We follow the USB HID naming/enumeration convention.

## Keymap files

A keymap is written in the TOML language.

All values inside `[keycodes]`, `[chars]` and `[modifier.*]` are either
- a direct mapping, or
- an array where the first entry denotes the keycode and the second entry is when modified using `Shift` (left/right).

### Includes

You may include other keymaps with the key `include`. It takes a list of keymap names that will merge sequentially,
meaning that any listed map will overwrite preceding mappings.

```toml
include = ["list", "of", "pre-included", "maps"]
```

### Keycodes

The table `keycodes` contains the mapping of keycodes to keycodes. Keycodes are a `u16`:

```toml
[keycodes]
# key with shift mapping
12 = [12, 12]

# no shift mapping
12 = 12

# ...
```

### Unicode chars

The table `chars` contains the mapping of keycodes to unicode chars. Unicode chars are a `u32`:

```toml
[chars]
# key with shift mapping
4 = ['a', 'A']

# or no shift mapping
4 = 'a'

# ...
```

Depending on the layout you can therefore switch keys like `y` to `z` in the [qwertz layout](./res/qwertz.toml).

### Modifiers

The modifiers work the other way around to be able to give descriptions of targeted keycodes by a list of modifier keys.
Modifiers are either:

- `Control`
- `Shift`
- `GUI` (rusty crab/tux/windows/cmd key)
- `Alt` (left alt)
- `AltGr` (right alt)

The deserialization is case-sensitive.

```toml
[modifiers.keycode]
41 = ["Control", 184] # ESC = ctrl + {
# ...

[modifiers.chars]
'�' = ["Alt", 31] # � = alt + 2
# ...
```

## TODOs

This crate does not take into account dead keys and only returns the immediate representation. This might change in the
future if it is deemed to be the responsibility of this crate.