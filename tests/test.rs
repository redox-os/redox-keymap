use redox_keymap::*;

#[test]
fn load_numbers() {
    let path = "./res/numbers.toml";
    let keymap = Keymap::load_keymap(path).unwrap();

    assert_eq!(10, keymap.num_keycodes());

    test_numbers(&keymap);
}

#[test]
fn load_function_row() {
    let path = "./res/function-row.toml";
    let keymap = Keymap::load_keymap(path).unwrap();

    assert_eq!(28, keymap.num_keycodes());

    test_function_row(&keymap);
}

#[test]
fn load_numpad() {
    let path = "./res/numpad.toml";
    let keymap = Keymap::load_keymap(path).unwrap();

    // #keys + #{shift keys}
    assert_eq!(17 + 11, keymap.num_keycodes());

    test_numpad(&keymap);
}

#[test]
fn load_navigation_arrows() {
    let path = "./res/navigation.toml";
    let keymap = Keymap::load_keymap(path).unwrap();

    assert_eq!(10, keymap.num_keycodes());

    test_navigation_arrows(&keymap);
}

#[test]
fn load_modifiers() {
    let path = "./res/modifiers.toml";
    let keymap = Keymap::load_keymap(path).unwrap();

    assert_eq!(13, keymap.num_keycodes());

    test_modifiers(&keymap);
}

#[test]
fn load_qwerty() {
    let path = "./res/qwerty.toml";
    let keymap = Keymap::load_keymap(path).unwrap();

    test_qwerty_layout(&keymap);
    test_numbers(&keymap);
    test_function_row(&keymap);
    test_numpad(&keymap);
    test_navigation_arrows(&keymap);
}

#[test]
fn load_qwertz() {
    let path = "./res/qwertz.toml";
    let keymap = Keymap::load_keymap(path).unwrap();

    test_qwertz_layout(&keymap);
    test_numbers(&keymap);
    test_function_row(&keymap);
    test_numpad(&keymap);
    test_navigation_arrows(&keymap);
}
