//! Complete test for the backup keymap.
use redox_input::{KeyModifier, Keycode};
use redox_keymap::*;

#[test]
fn test_keycode_mapping() {
    let keymap = Keymap::backup();

    for i in 0..u16::MAX {
        let keycode = Keycode::from(i);
        match i {
            4..=29 | 45..=49 | 51..=56 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            30..=44 | 57..=82 | 84..=88 | 93 | 101 | 102 | 104..=115 | 224..=229 | 231 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                test_get_keycode_equals_none(&keymap, keycode, &[KeyModifier::empty()]);
            }
            83 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(156)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            89 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(77)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            90 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(81)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            91 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(78)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            92 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(80)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            94 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(79)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            95 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(74)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            96 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(82)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            97 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(75)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            98 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(73)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            99 => {
                assert_eq!(
                    Some(keycode),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(Keycode::from(42)),
                    keymap.get_keycode(keycode, KeyModifier::SHIFT)
                );
                test_get_keycode_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            230 => {
                assert_eq!(
                    Some(Keycode::from(226)),
                    keymap.get_keycode(keycode, KeyModifier::empty())
                );
                test_get_keycode_equals_none(&keymap, keycode, &[KeyModifier::empty()]);
            }
            _ => {
                println!("{}", i);
                test_get_keycode_equals_none(&keymap, keycode, &[])
            }
        }
    }
}

#[test]
fn test_char_mapping() {
    let keymap = Keymap::backup();

    for i in 0..u16::MAX {
        let keycode = Keycode::from(i);
        match i {
            4..=29 => {
                let lower_case = char::from(97 - 4 + i as u8); // 'a' starts at 97
                let upper_case = char::from(65 - 4 + i as u8); // 'A' starts at 65
                assert_eq!(
                    Some(lower_case),
                    keymap.get_char(keycode, KeyModifier::empty())
                );
                assert_eq!(
                    Some(upper_case),
                    keymap.get_char(keycode, KeyModifier::SHIFT)
                );
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            30 => {
                assert_eq!(Some('1'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('!'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            31 => {
                assert_eq!(Some('2'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('@'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            32 => {
                assert_eq!(Some('3'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('#'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            33 => {
                assert_eq!(Some('4'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('$'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            34 => {
                assert_eq!(Some('5'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('%'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            35 => {
                assert_eq!(Some('6'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('^'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            36 => {
                assert_eq!(Some('7'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('&'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            37 => {
                assert_eq!(Some('8'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('*'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            38 => {
                assert_eq!(Some('9'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('('), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            39 => {
                assert_eq!(Some('0'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some(')'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            40 => {
                assert_eq!(Some('\n'), keymap.get_char(keycode, KeyModifier::empty()));
                test_get_char_equals_none(&keymap, keycode, &[KeyModifier::empty()]);
            }
            43 => {
                assert_eq!(Some('\t'), keymap.get_char(keycode, KeyModifier::empty()));
                test_get_char_equals_none(&keymap, keycode, &[KeyModifier::empty()]);
            }
            44 => {
                assert_eq!(Some(' '), keymap.get_char(keycode, KeyModifier::empty()));
                test_get_char_equals_none(&keymap, keycode, &[KeyModifier::empty()]);
            }
            45 => {
                assert_eq!(Some('-'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('_'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            46 => {
                assert_eq!(Some('='), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('+'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            47 => {
                assert_eq!(Some('['), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('{'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            48 => {
                assert_eq!(Some(']'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('}'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            49 => {
                assert_eq!(Some('\\'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('|'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            51 => {
                assert_eq!(Some(';'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some(':'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            52 => {
                assert_eq!(Some('\''), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('\"'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            53 => {
                assert_eq!(Some('`'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('~'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            54 => {
                assert_eq!(Some(','), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('<'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            55 => {
                assert_eq!(Some('.'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('>'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            56 => {
                assert_eq!(Some('/'), keymap.get_char(keycode, KeyModifier::empty()));
                assert_eq!(Some('?'), keymap.get_char(keycode, KeyModifier::SHIFT));
                test_get_char_equals_none(
                    &keymap,
                    keycode,
                    &[KeyModifier::empty(), KeyModifier::SHIFT],
                );
            }
            _ => test_get_char_equals_none(&keymap, keycode, &[]),
        }
    }
}
